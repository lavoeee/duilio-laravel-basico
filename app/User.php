<?php

namespace Course;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'type'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profile(){
        return $this->hasOne('Course\UserProfile');
    }

    public function getFullNameAttribute(){
        return $this->first_name.'  '.$this->last_name;
    }

    public function setPasswordAttribute($value){
        if(!empty($value))
            $this->attributes['password'] = \Hash::make($value);

    }

    public function scopeName($query, $name){
        if(trim($name) != "")
            $query->where(\DB::raw("CONCAT(first_name,' ',last_name)"), "LIKE", "%".$name."%");
    }

    public function is($type){
        return $type === $this->type;

    public function scopeType($query, $type){
        $types = config('htmls.selects.types_profiles.types');

        if($type != "" && isset($types[$type])):
            $query->where('type', $type);
        endif;
    }

    public static function filterAndPaginate($name, $type){
        return User::name($name)->type($type)->orderBy('id','DESC')->paginate();
    }
}
