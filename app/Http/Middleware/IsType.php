<?php

namespace Course\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

abstract class IsType
{

    public function handle($request, Closure $next)
    {
    	if(Auth::User()->type != $this->getType()):
		    if ($request->ajax() || $request->wantsJson()):
		        return response('Unauthorized.', 401);
		    else:
		       	return redirect()->to('home');
		    endif;	
    	endif;

        return $next($request);
    }

    abstract public function getType();
}
