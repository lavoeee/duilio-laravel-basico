<?php

namespace Course\Http\Controllers\Admin;

use Course\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Course\Http\Requests;

use Course\User;

use Course\Http\Requests\CreateUserRequest;
use Course\Http\Requests\EditUserRequest;

use Session;

class UsersController extends Controller{

    public function index(Request $request)
    {
        $users = User::filterAndPaginate($request->get('name'), $request->get('type'));
        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(CreateUserRequest $request)
    {
        $user = new User($request->all());
        $user->save(); 

        return \Redirect::route('admin.users.index');
        //return redirect()->route('admin.users.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('admin.users.edit',compact('user'));
    }

    public function update(EditUserRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $user->fill($request->all());
        $user->save();

        return redirect()->back(); 
    }

    public function destroy($id, Request $request)
    {
        $user  = User::findOrFail($id);
        $message =  $user->full_name.'  fue eliminado';
        $user->delete();

        if($request->ajax()):
            return response()->json(['message' => $message, 'id' => $id]);
        endif;
        
        Session::flash('message', $message);
        return redirect()->route('admin.users.index');
    }
}