<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::controllers([
	'users' => 'UsersController'
	]);

Route::get('home', [
    'as' => 'home',
    'uses' => 'HomeController@index'
]);

Route::group(['prefix' => 'admin' , 'middleware' => 'auth','namespace' => 'Admin'], function(){
    Route::resource('users','UsersController');
});

Route::get('login', ['uses' => 'Auth\AuthController@showLoginForm', 'as' => 'login']);
Route::post('login', ['uses' => 'Auth\AuthController@login', 'as' => 'login']);

Route::get('logout', ['uses' => 'Auth\AuthController@logout', 'as' => 'logout']);

// Registration routes...
Route::get('register', ['uses' => 'Auth\AuthController@getRegister', 'as' => 'register']);
Route::post('register', ['uses' => 'Auth\AuthController@showRegistrationForm', 'as' => 'register']);

// Password reset link request routes...
//Route::get('password/email', 'Auth\PasswordController@register');
Route::post('password/email', ['uses' => 'Auth\PasswordController@sendResetLinkEmail', 'as' =>'password-email' ]);

// Password reset routes...
Route::get('password/reset/{token?}', ['uses' => 'Auth\PasswordController@showResetForm', 'as' =>  'password-reset']);
Route::post('password/reset', ['uses' => 'Auth\PasswordController@reset', 'as' => 'password-reset' ]);