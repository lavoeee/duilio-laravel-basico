<?php

namespace Course\Http\Requests;

use Course\Http\Requests\Request;
use Route;

class EditUserRequest extends Request
{

    public function __construct(){
        $this->user_id = Route::getCurrentRoute()->parameters()['users'];
    }

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'first_name' => 'required',
            'last_name'  => 'required',
            'email'     => 'required|unique:users,email,'.$this->user_id,
            'type'      => 'required|in:user,admin'
        ];
    }
}
