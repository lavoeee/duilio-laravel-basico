<?php
use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder{

	public function run(){
		\DB::table('users')->insert(array(
			'first_name'	=> 'Hector',
			'last_name'	=> 'Lavoe',
			'email' => 'hlavoe@gmail.com',
			'password' => \Hash::make('secret'),
			'type'	=> 'admin'
			));

		\DB::table('user_profiles')->insert(array(
			'user_id' => 1 , 
			'birthdate' => '1983/09/23'
			));

	}

}