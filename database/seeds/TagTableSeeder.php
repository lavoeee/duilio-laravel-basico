<?php
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class TagTableSeeder extends Seeder{

	public function run(){

		$faker = Faker::create();

		for ($i=0; $i <  30; $i++):
			
			\DB::table('tags')->insert(array(
				'name' => $faker->unique()->word,
				'description'	=> $faker->paragraph(rand(1,2)),
				));
		endfor;
	}

}