@extends('layout')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading"> Editar Usuario:  {{ $user->first_name }}</div>
				<div class="panel-body">
					@include('errors.messages')
					{!! Form::model($user, ['route' => ['admin.users.update', $user->id], 'method' => 'PUT' ])!!}
						@include('admin.users.partials.fields')
						<button class="btn btn-default" type="submit">Actualizar</button>
					{!! Form::close() !!}
				</div>
			</div>
			@include('admin.users.partials.delete')
		</div>
	</div>
</div>
@endsection