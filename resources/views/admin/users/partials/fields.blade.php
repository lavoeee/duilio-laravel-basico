 						<div class="form-group">
							{!! Form::label('email', 'Correo Electrónico') !!}
							{!! Form::text('email', null, ['class' => 'form-control' , 'placeholder' => 'Ingrese un correo']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('password', 'Password') !!}
							{!! Form::password('password', ['class' => 'form-control']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('first_name', 'Primer Nombre') !!}
							{!! Form::text('first_name', null, ['class' => 'form-control']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('last_name', 'Apellido') !!}
							{!! Form::text('last_name', null, ['class' => 'form-control']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('type', 'Tipo de usuario') !!}
							{!! Form::select('type', ['' => 'Seleccione tipo ', 'user' => 'Usuario', 'admin'=> 'Administrador'], null, ['class' => 'form-control']) !!}
						</div>