{!! Form::model($user, ['route' => ['admin.users.destroy', $user->id], 'method' => 'DELETE' ])!!}
	<button class="btn btn-danger" onclick="return confirm('Seguro que desea eliminar?')"type="submit">Eliminar usuario</button>
{!! Form::close() !!}
