@extends('layout')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading"> Usuarios</div>
				<div class="panel-body">
					@include('admin.users.partials.searcher')
					<p>
						<a href="{{ route('admin.users.create') }}" class="btn btn-info" role="button"> Nuevo Usuario </a>
					</p>
					<p>Hay {{ $users->total() }} usuarios </p>
					@include('admin.users.partials.table')
					{!! $users->appends(Request::only(['name','type']))->render() !!}
				</div>
			</div>
		</div>
	</div>
</div>
{!! Form::open(['route' => ['admin.users.destroy', ':USER_ID'], 'method' => 'DELETE','id'=>'form-delete' ])!!}
{!! Form::close() !!}
@endsection


@section('scripts')
	<script>
		$(document).ready(function(){
			$('.btn-delete').click(function(e){
				e.preventDefault();
				var row = $(this).parents('tr');
				var id = row.data('id');
				var form = $('#form-delete');
				var url =  form.attr('action').replace(':USER_ID',id);
				var data = form.serialize();

				$.post(url,data, function(result){
					alert(result.message);
					row.fadeOut();
				}).fail(function(){
					alert('El usuario no fue eliminado');
					row.show();
				});
			});
		});
	</script>
@endsection